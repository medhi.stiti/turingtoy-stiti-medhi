from typing import (
    Dict,
    List,
    Optional,
    Tuple,
)

import poetry_version

__version__ = poetry_version.extract(source_file=__file__)


def run_turing_machine(
    def run_turing(machine, input):
    tape = list(input)
    head_position = 0  # We start from the leftmost position
    state = machine['start state']
    execution_history = []

    while state not in machine['final states']:
        symbol = tape[head_position] if 0 <= head_position < len(tape) else machine['blank']
        action = machine['table'][state].get(symbol, machine['table'][state].get(machine['blank']))

        # Log the execution step
        execution_history.append({
            'state': state,
            'reading': symbol,
            'position': head_position,
            'memory': ''.join(tape),
            'transition': action,
        })

        # Perform the action
        if 'write' in action:
            if 0 <= head_position < len(tape):
                tape[head_position] = action['write']
            else:
                tape.append(action['write'])

        if 'L' in action:
            head_position -= 1
            if head_position < 0:  # handle moving left from the leftmost position
                tape.insert(0, machine['blank'])
                head_position = 0
        elif 'R' in action:
            head_position += 1
            if head_position >= len(tape):  # handle moving right from the rightmost position
                tape.append(machine['blank'])

        state = action.get('next state', state)

    output = ''.join(tape).rstrip()  # Remove trailing blanks
    return {
        'machine': machine,
        'input': input,
        'output': output,
        'execution_history': execution_history,
    }
